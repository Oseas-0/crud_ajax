package com.personal.controller;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.personal.entidades.Consulta;
import com.personal.services.ConsultaService;

@Controller
@RequestMapping("consulta")
public class ConsultaController {

	@Autowired
	ConsultaService DaoConsulta;
	
	@GetMapping(value="lista", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Consulta> Lista(){
		return (List<Consulta>) DaoConsulta.Lista();
	}
	
	@GetMapping("guardar")
	@ResponseBody
	public HashMap<String, String> Guardar(@RequestParam Date fecha, @RequestParam String sintomas, @RequestParam String diagnostico, @RequestParam Integer doctor, @RequestParam Integer paciente){
		
		Consulta c = new Consulta();
		
		HashMap<String, String> json = new HashMap<>();
		c.setFecha(fecha);
		c.setSintomas(sintomas);
		c.setDiagnostico(diagnostico);
		c.setDoctor(DaoConsulta.getdoctor(doctor));
		c.setPaciente(DaoConsulta.getpaciente(paciente));
		
		try {
			DaoConsulta.SaveOrUpdate(c);
			json.put("estado", "OK");
			json.put("mensaje", "Registro guardado");
			return json;
			
		}catch(Exception e) {
			json.put("estado", "ERROR");
			json.put("mensaje", "Registro no guardado"+e.getMessage());
			return json;
		}
	}
	
	@GetMapping(value="actualizar/{id}")
	@ResponseBody
	public HashMap<String, String> Actualizar(@RequestParam Integer id, @RequestParam Date fecha, @RequestParam String sintomas, @RequestParam String diagnostico, @RequestParam Integer doctor, @RequestParam Integer paciente){
		Consulta c = new Consulta();
		
		HashMap<String, String> json = new HashMap<>();
		
		c.setId_consulta(id);
		c.setFecha(fecha);
		c.setSintomas(sintomas);
		c.setDiagnostico(diagnostico);
		c.setDoctor(DaoConsulta.getdoctor(doctor));
		c.setPaciente(DaoConsulta.getpaciente(paciente));
		
		try {
			DaoConsulta.SaveOrUpdate(c);
			json.put("estado", "OK");
			json.put("mensaje", "Registro actualizado");			 
			return json;
		} catch (Exception e) {
			json.put("estado", "ERROR");
			json.put("mensaje", "Registro no actualizado"+e.getMessage());
			return json;
		}
	}
	
	@GetMapping(value="delete/{id}")
	@ResponseBody
	public HashMap<String,String> Eliminar (@PathVariable Integer id) {
			 
		HashMap<String, String> json=new HashMap<>();
		        
		try {
		    Consulta consulta=DaoConsulta.getconsulta(id);
		    DaoConsulta.Eliminar(consulta);
			json.put("estado", "OK");
			json.put("mensaje", "Registro eliminado");
			return json;
		        	
		}catch(Exception e) {
			json.put("estado", "ERROR");
			json.put("mensaje", "Registro no eliminado"+e.getMessage());
			return json;
			}
		}
}
