package com.personal.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.personal.entidades.Doctor;
import com.personal.services.DoctorService;

@Controller
@RequestMapping("doctor")
public class DoctorController {
	
	//Repositorio para el manejo de datos
	@Autowired
	DoctorService daoDoctor;
	
	//Metodo para listar y mostrarlo en JSON, o un tabla
	@GetMapping(value="all",produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Doctor> Listar() {
		return (List<Doctor>) daoDoctor.Lista();
	}
	
	//Metodo para guardar
	@GetMapping(value="save")
	@ResponseBody
	public HashMap<String, String> Guardar(@RequestParam String nombre, @RequestParam String direccion, @RequestParam Integer especialidad) {
		
		//Creando Objeto de la entidad Doctor
		Doctor d=new Doctor();
			 
		HashMap<String, String> jsonReturn=new HashMap<>();
		//Obteniendo datos
		d.setNombre(nombre);
		d.setDireccion(direccion);
		d.setEspecialidad(daoDoctor.especialidad(especialidad));
			 
		//Manejando Excepcion de Errores
		try {
			//Guardando Registro
			daoDoctor.saveOrUpdate(d);
			jsonReturn.put("estado", "OK");
			jsonReturn.put("mensaje", "Registro guardado");
				 
			return jsonReturn;
		}catch(Exception e) {
			jsonReturn.put("estado", "ERROR");
			jsonReturn.put("mensaje", "Registro no guardado"+e.getMessage());
			return jsonReturn;
		}
			 
		}
	
	//Metodo para modificar
	@GetMapping(value="update/{id}")
	@ResponseBody
	public HashMap<String, String> Actualizar(@RequestParam Integer id, @RequestParam String nombre, @RequestParam String direccion, @RequestParam Integer especialidad) {
		//Creando Objeto de la entidad Doctor
		Doctor d=new Doctor();
				 
		HashMap<String, String> jsonReturn=new HashMap<>();
		//asignado datos al objeto de doctor
		d.setId_doctor(id);
		d.setNombre(nombre);
		d.setDireccion(direccion);
		d.setEspecialidad(daoDoctor.especialidad(especialidad));
				 
		//Manejando Excepcion de Errores
		try {
			daoDoctor.saveOrUpdate(d);//Guardando Modficacion
			jsonReturn.put("estado", "OK");
			jsonReturn.put("mensaje", "Registro actualizado");			 
			return jsonReturn;
					 
		}catch(Exception e) {
			jsonReturn.put("estado", "ERROR");
			jsonReturn.put("mensaje", "Registro no actualizado"+e.getMessage());
			return jsonReturn;
		}
				 
	}
		 
	//Metodo para eliminar
	@GetMapping(value="delete/{id}")
	@ResponseBody
	public HashMap<String,String> Eliminar (@PathVariable Integer id) {
			 
		HashMap<String, String> jsonReturn=new HashMap<>();
		        
		try {
			//Buscando registro
		    Doctor doctor=daoDoctor.doctor(id);
		    //Eliminando registro
		    daoDoctor.eliminar(doctor);
			jsonReturn.put("estado", "OK");
			jsonReturn.put("mensaje", "Registro eliminado");
			return jsonReturn;
		        	
		}catch(Exception e) {
			jsonReturn.put("estado", "ERROR");
			jsonReturn.put("mensaje", "Registro no guardado"+e.getMessage());
			return jsonReturn;
			}
		}
}
