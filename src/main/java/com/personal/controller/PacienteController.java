package com.personal.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.personal.entidades.Paciente;
import com.personal.repositorios.IPacienteRepository;

@Controller
@RequestMapping("paciente")
public class PacienteController {

	@Autowired
	IPacienteRepository rPaciente;
	
	@GetMapping(value = "lista", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Paciente> Listar(){
		return (List<Paciente>) rPaciente.findAll();
	}
	
	@GetMapping("guardar")
	@ResponseBody
	public HashMap<String, String> Guardar(@RequestParam String nombre, String direccion) {
		
		Paciente p=new Paciente();
			 
		HashMap<String, String> jsonReturn=new HashMap<>();
		p.setNombre(nombre);
		p.setDireccion(direccion);
			 
		try {
			rPaciente.save(p);
			jsonReturn.put("Estado", "OK");
			jsonReturn.put("Mensaje", "Registro guardado");
				 
			return jsonReturn;
		}catch(Exception e) {
			jsonReturn.put("Estado", "ERROR");
			jsonReturn.put("Mensaje", "Registro no guardado"+e.getMessage());
			return jsonReturn;
			}
		}
	
	@GetMapping(value="modificar/{id_paciente}")
	@ResponseBody
	public HashMap<String, String> Actualizar(@RequestParam Integer id_paciente, @RequestParam String nombre, @RequestParam String direccion) {

		Paciente p=new Paciente();
				 
		HashMap<String, String> jsonReturn=new HashMap<>();
		p.setId_paciente(id_paciente);
		p.setNombre(nombre);
		p.setDireccion(direccion);
				 
		try {
			rPaciente.save(p);
			jsonReturn.put("Estado", "OK");
			jsonReturn.put("Mensaje", "Registro actualizado");			 
			return jsonReturn;
					 
		}catch(Exception ex) {
			jsonReturn.put("Estado", "ERROR");
			jsonReturn.put("Mensaje", "Registro no actualizado"+ex.getMessage());
			return jsonReturn;
		}
				 
	}

	@GetMapping(value="eliminar/{id_paciente}")
	@ResponseBody
	public HashMap<String,String> Eliminar (@PathVariable Integer id_paciente) {
			 
		HashMap<String, String> jsonReturn=new HashMap<>();
		        
		try {
			Paciente p = rPaciente.findById(id_paciente).get();
			rPaciente.delete(p);
			jsonReturn.put("Estado", "OK");
			jsonReturn.put("Mensaje", "Registro eliminado");
			return jsonReturn;
		        	
		}catch(Exception ex) {
			jsonReturn.put("Estado", "ERROR");
			jsonReturn.put("Mensaje", "Registro no guardado"+ex.getMessage());
			return jsonReturn;
			}
		}
}
