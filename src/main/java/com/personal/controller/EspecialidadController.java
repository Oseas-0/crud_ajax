package com.personal.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.personal.entidades.Especialidad;
import com.personal.repositorios.IEspecialidadRepository;

@Controller
@RequestMapping("especialidad")
public class EspecialidadController {
	
	@Autowired
	IEspecialidadRepository rEspecialidad;
	
	@GetMapping(value="lista", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Especialidad> Listar() {
		return (List<Especialidad>) rEspecialidad.findAll();
	}
	
	@GetMapping("guardar")
	@ResponseBody
	public HashMap<String, String> Guardar(@RequestParam String especialidad) {
		
		Especialidad e=new Especialidad();
			 
		HashMap<String, String> jsonReturn=new HashMap<>();
		e.setEspecialidad(especialidad);
			 
		try {
			rEspecialidad.save(e);
			jsonReturn.put("estado", "OK");
			jsonReturn.put("mensaje", "Registro guardado");
				 
			return jsonReturn;
		}catch(Exception ex) {
			jsonReturn.put("estado", "ERROR");
			jsonReturn.put("mensaje", "Registro no guardado"+ex.getMessage());
			return jsonReturn;
			}
		}
	
	@GetMapping(value="modificar/{id_especialidad}")
	@ResponseBody
	public HashMap<String, String> Actualizar(@RequestParam Integer id_especialidad, @RequestParam String especialidad) {

		Especialidad e=new Especialidad();
				 
		HashMap<String, String> jsonReturn=new HashMap<>();
		e.setId_especialidad(id_especialidad);
		e.setEspecialidad(especialidad);
				 
		try {
			rEspecialidad.save(e);
			jsonReturn.put("estado", "OK");
			jsonReturn.put("mensaje", "Registro actualizado");			 
			return jsonReturn;
					 
		}catch(Exception ex) {
			jsonReturn.put("estado", "ERROR");
			jsonReturn.put("mensaje", "Registro no actualizado"+ex.getMessage());
			return jsonReturn;
		}
				 
	}
	
	@GetMapping(value="eliminar/{id_especialidad}")
	@ResponseBody
	public HashMap<String,String> Eliminar (@PathVariable Integer id_especialidad) {
			 
		HashMap<String, String> jsonReturn=new HashMap<>();
		        
		try {
			Especialidad e = rEspecialidad.findById(id_especialidad).get();
			rEspecialidad.delete(e);
			jsonReturn.put("estado", "OK");
			jsonReturn.put("mensaje", "Registro eliminado");
			return jsonReturn;
		        	
		}catch(Exception ex) {
			jsonReturn.put("estado", "ERROR");
			jsonReturn.put("mensaje", "Registro no guardado"+ex.getMessage());
			return jsonReturn;
			}
		}
}
