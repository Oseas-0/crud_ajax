package com.personal.entidades;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "doctores")
public class Doctor {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id_doctor;
	
	private String nombre;
	
	private String direccion;
	
	@JoinColumn(name="id_especialidad",referencedColumnName = "id_especialidad",nullable = false)
	@ManyToOne(optional = true,fetch = FetchType.EAGER)
	private Especialidad especialidad;
	
	 public Doctor() {
		// TODO Auto-generated constructor stub
	}

	public Doctor(Integer id_doctor, String nombre, String direccion, Especialidad especialidad) {
		super();
		this.id_doctor = id_doctor;
		this.nombre = nombre;
		this.direccion = direccion;
		this.especialidad = especialidad;
	}

	public Integer getId_doctor() {
		return id_doctor;
	}

	public void setId_doctor(Integer id_doctor) {
		this.id_doctor = id_doctor;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Especialidad getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(Especialidad especialidad) {
		this.especialidad = especialidad;
	}

}
