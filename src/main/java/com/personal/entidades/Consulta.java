package com.personal.entidades;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "consultas")
public class Consulta {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id_consulta;
	
	private Date fecha;
	
	private String sintomas;
	
	private String diagnostico;
	
	@JoinColumn(name="id_doctor",referencedColumnName = "id_doctor",nullable = false)
	@ManyToOne(optional = true, fetch = FetchType.EAGER)
	private Doctor doctor;
	
	@JoinColumn(name="id_paciente",referencedColumnName = "id_paciente",nullable = false)
	@ManyToOne(optional = true, fetch = FetchType.EAGER)
	private Paciente paciente;
	
	public Consulta() {
		// TODO Auto-generated constructor stub
	}

	public Consulta(Integer id_consulta, Date fecha, String sintomas, String diagnostico, Doctor doctor,
			Paciente paciente) {
		super();
		this.id_consulta = id_consulta;
		this.fecha = fecha;
		this.sintomas = sintomas;
		this.diagnostico = diagnostico;
		this.doctor = doctor;
		this.paciente = paciente;
	}

	public Integer getId_consulta() {
		return id_consulta;
	}

	public void setId_consulta(Integer id_consulta) {
		this.id_consulta = id_consulta;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getSintomas() {
		return sintomas;
	}

	public void setSintomas(String sintomas) {
		this.sintomas = sintomas;
	}

	public String getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

}
