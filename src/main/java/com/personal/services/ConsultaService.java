package com.personal.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.personal.entidades.Consulta;
import com.personal.entidades.Doctor;
import com.personal.entidades.Paciente;
import com.personal.repositorios.IConsultaRepository;
import com.personal.repositorios.IDoctorRepository;
import com.personal.repositorios.IPacienteRepository;

@Service
public class ConsultaService {

	@Autowired
	IDoctorRepository rDoctor;
	
	@Autowired
	IConsultaRepository rConsulta;
	
	@Autowired
	IPacienteRepository rPaciente;
	
	public List<Consulta> Lista(){
		return (List<Consulta>) rConsulta.findAll();
	}
	
	public Boolean SaveOrUpdate(Consulta consulta) {
		try {
			rConsulta.save(consulta);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public Boolean Eliminar(Consulta consu) {
		try {
			rConsulta.delete(consu);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public Doctor getdoctor(Integer id_doctor) {
		return rDoctor.findById(id_doctor).get();
	}
	
	public Consulta getconsulta(Integer id_consulta) {
		return rConsulta.findById(id_consulta).get();
	}
	
	public Paciente getpaciente(Integer id_paciente) {
		return rPaciente.findById(id_paciente).get();
	}

}
