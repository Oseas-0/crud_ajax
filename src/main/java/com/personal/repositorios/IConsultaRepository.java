package com.personal.repositorios;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.personal.entidades.Consulta;

@Repository
public interface IConsultaRepository extends CrudRepository<Consulta, Integer>{

}
