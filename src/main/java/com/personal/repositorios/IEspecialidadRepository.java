package com.personal.repositorios;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.personal.entidades.Especialidad;

@Repository
public interface IEspecialidadRepository extends CrudRepository<Especialidad, Integer>{

}
