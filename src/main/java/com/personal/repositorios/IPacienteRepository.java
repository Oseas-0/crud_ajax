package com.personal.repositorios;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.personal.entidades.Paciente;

@Repository
public interface IPacienteRepository extends CrudRepository<Paciente, Integer>{

}
